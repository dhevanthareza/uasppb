<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
$router->group(['prefix' => '/auth'], function () use ($router) {
    $router->post('/login', 'AuthController@login');
    $router->post('/register', 'AuthController@register');
    $router->post('/update', 'AuthController@update');
});
$router->group(['prefix' => '/barang', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'BarangController@fetch');
});
$router->group(['prefix' => '/transaction', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'TransactionController@get');
    $router->post('/', 'TransactionController@create');
    $router->post('/{transaction_id}/bukti-bayar', 'TransactionController@uploadBuktiBayar');
});
