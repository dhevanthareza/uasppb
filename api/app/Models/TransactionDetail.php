<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = 'penjualan_detail';
    protected $guarded = [];
    public $timestamps = false;
}
