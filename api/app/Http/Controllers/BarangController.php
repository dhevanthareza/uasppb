<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{
    public function fetch() {
        $products = Barang::get();
        return response()->json($products, 200);
    }
}
