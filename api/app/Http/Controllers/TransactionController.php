<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function create(Request $request)
    {
        $body = $request->all();
        $transaction_payload = [
            'tanggal' => date('Y-m-d'),
            'total_pembelian' => $body['total_pembelian'],
            'ongkir' => $body['ongkir'],
            'total' => $body['total'],
            'kota' => $body['kota'],
            'provinsi' => $body['provinsi'],
            'status_pembayaran' => 0,
            'id_user' => Auth::user()->id
        ];
        $transaction = Transaction::create($transaction_payload);
        $products = array_map(function ($el) use ($transaction) {
            return [
                'kd_barang' => $el['kd_brg'],
                'harga' => $el['harga'],
                'id_penjualan' => $transaction->id
            ];
        }, $body['products']);
        TransactionDetail::insert($products);
        $transaction = Transaction::where('id', $transaction->id)->first();
        return response()->json($transaction, 200);
    }

    public function get()
    {
        $transaction = Transaction::where('id_user', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return response()->json($transaction, 200);
    }

    public function uploadBuktiBayar(Request $request, $transaction_id)
    {
        $filename =  "Bukti Bayar" . "-" . $transaction_id;
        $foto_bukti_bayar = 'images/bukti_bayar/' . $filename;
        $request->file('bukti_bayar')->move('../images/bukti_bayar/', $filename);

        Transaction::where('id', $transaction_id)->update(['bukti_bayar' => $foto_bukti_bayar, 'status_pembayaran' => 1]);
        $transaction = Transaction::where('id', $transaction_id)->first();
        return response()->json($transaction);
    }
}
