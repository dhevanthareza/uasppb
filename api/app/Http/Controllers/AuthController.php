<?php

namespace App\Http\Controllers;

use App\Helper\JwtHelper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('user_id', $request->input('user_id'))->where('password', md5($request->input('password')))->first();
        if ($user == null) {
            return response()->json(['message' => "User tidak ditemukan atau password salah"], 400);
        }
        $token = JwtHelper::encode($user);
        $user['token'] = $token;
        return response()->json($user, 200);
    }

    public function register(Request $request)
    {
        $user_create = User::create(
            [
                'user_id' => $request->input('username'),
                'name' => $request->input('name'),
                'password' => md5($request->input('password')),
                'hak_akses' => 2
            ]
        );
        $user = User::where('id', $user_create->id)->first();
        $token = JwtHelper::encode($user);
        $user = $user->toArray();
        $user['token'] = $token;
        return response()->json($user, 200);
    }

    public function update(Request $request)
    {
        $payload = [
            'user_id' => $request->input('username'),
            'name' => $request->input('name')
        ];
        if ($request->input('password') != null) {
            $payload['password'] = md5($request->input('password'));
        }
        User::where('id', Auth::user()->id)->update($payload);
        $user = User::where('id', Auth::user()->id)->first();
        $token = JwtHelper::encode($user);
        $user = $user->toArray();
        $user['token'] = $token;
        return response()->json($user, 200);
    }
}
