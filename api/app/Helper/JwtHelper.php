<?php
namespace App\Helper;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtHelper {
    public static function encode($payload) {
        $jwt = JWT::encode($payload, 'secret', 'HS256');
        return $jwt;
    }
    public static function decode($jwt) {
        $jwt = JWT::decode($jwt, new Key('secret', 'HS256'));
        return $jwt;
    }
}