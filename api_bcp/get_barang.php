<?php
require_once('./../koneksi_ip.php');
$query = "SELECT * FROM barang order by kd_brg";
$myArray = array();
if ($result = mysqli_query($conn, $query)) {
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $myArray[] = $row;
    }
    mysqli_close($conn);
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($myArray);
}
